<!DOCTYPE HTML>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=no">
	<meta name="theme-color" content="#e1e2e1" media="(prefers-color-scheme: light)">
    <meta name="theme-color" content="#e1e2e1" media="(prefers-color-scheme: dark)">
	<link href="https://fonts.googleapis.com/css?family=Annie+Use+Your+Telescope|raleway:100" rel="stylesheet">
	<link rel="icon" type="image/png" href="/favicon.ico">
	<link rel="stylesheet" type="text/css" href="contents/jq/jquery-ui.min.css">
	<link rel="stylesheet" href="contents/css/style.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pdf-lib@1.4.0/dist/pdf-lib.min.js"></script>
	<script src="https://unpkg.com/downloadjs@1.4.7"></script>
	<script src="contents/jq/jquery-ui.min.js"></script>
		<script type="text/javascript">
		$(function() {
			var availableTags = [
				"Steak",
				"Jambon",
				"Jambon cru",
				"Poulet",
				"Saucisse",
				"Lardons",
				"Saucisse",
				"Ail",
				"Carottes",
				"Champignons",
				"Tomates",
				"Salade",
				"Patates",
				"Avocats",
				"Bananes",
				"Herbes Provence",
				"Paprika",
				"Curry",
				"Basilic",
				"Cornichons",
				"Gruyère-rapé",
				"Gruyère",
				"Lait",
				"Yaourts blanc",
				"Yaourts",
				"Beurre",
				"Emmental",
				"Mozza'",
				"Creme fraiche",
				"Pain",
				"Pâte feuilletée",
				"Pâte brisée",
				"Farine",
				"Riz",
				"Pâtes",
				"Sucre",
				"Mayonnaise",
				"Ketchup",
				"Sel",
				"Poivre",
				"Sauce tomate",
				"Vinaigre",
				"Moutarde",
				"Bières",
				"Café",
				"Jus orange",
				"Thé",
				"Cacao",
				"Vin",
				"Shampooing",
				"Coton-tiges",
				"Pq",
				"Dentifrice",
				"Deodorant",
				"Liquide vaisselle",
				"Sacs poubelle",
				"Lessive",
				"Bouffe chats"
			];
			$("#tags").autocomplete({
				source: availableTags
			});
		});
	</script>

	<title>Courses</title>

</head>

<body>
	<section class="wrap">
		<button class="btn" onclick="createPdf()"><img class="pdf-button" src="contents/images/file-pdf.svg"/>PDF</button>
		<article class="task-list">

			<ul>
			<?php
				require("contents/functions.php");

				$query  = "SELECT * FROM tasks ORDER BY id";
				$result = queryMysql($query);
				$num    = $result->num_rows;

				if ($num>0) {
					while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
						$task_id   = $row['id'];
						$task_name = $row['task'];
						echo '<li>
										<img id="'.$task_id.'" class="neon-button" width="20px" src="contents/images/stabilo.png" />
										<span data-taskid="'.$task_id.'" style="cursor: pointer;">'.$task_name.'</span>
										<img id="'.$task_id.'" class="delete-button" width="15px" src="contents/images/close.svg" />
							  	</li>';
					}
				}
			?>
			</ul>

		</article>

		<form class="add-new-task ui-widget" autocomplete="off">
			<input type="text" id="tags" name="new-task" placeholder="Produits ..."/>
		</form>

	</section>

	<footer>
		<p id='horloge'></p>
	</footer>

	<script type="text/javascript" defer>
		add_task();
		delete_task();
		// ajout d'une tache
		function add_task() {
		  $('.add-new-task').submit(function() {
		    var new_task = $('.add-new-task input[name=new-task]').val();
		    if(new_task != '') {
		    	$.post('contents/add.php', { task: new_task }, (data) => {
		        $('.add-new-task input[name=new-task]').val('');
		        $(data).appendTo('.task-list ul').hide().fadeIn();
		        delete_task();
				// On recharge la page après avoir ajouté un élément
        		location.reload();
		      });
		  	}
		  return false; // etre sur de ne pas appeller 2 fois
		  });
		}
		// effacement d'une tache
		function delete_task() {
		  $('.delete-button').click(function() {
		    var current_element = $(this);
		    var id = $(this).attr('id');
		    $.post('contents/remove.php', { task_id: id }, function() {
		      current_element.parent().fadeOut("fast", function() {
		        $(this).remove();
		      });
		    });
		  });
		}

		// surligne les elements de la liste avec un click
		$(document).ready(function() {
    // Attachez l'événement click à chaque élément avec la classe "neon-button"
	    $('.neon-button').click(function() {
	        // Récupérez l'ID de l'élément cliqué
	        var id = $(this).attr('id');

	        // Sélectionnez l'élément <span> correspondant
	        var correspondingSpan = $('span[data-taskid="' + id + '"]');

	        // Vérifiez si l'élément a déjà la classe "neon"
	        if (correspondingSpan.hasClass('neon')) {
	            // Si oui, supprimez la classe "neon"
	            correspondingSpan.removeClass('neon');
	        } else {
	            // Sinon, ajoutez la classe "neon"
	            correspondingSpan.addClass('neon');
	        }
	    });
		});

		// barre les elements de la liste avec un click
		$('span').click(function() {
	    $(this).toggleClass('checked');
	  });

		// Creation de la liste en PDF
    const { PDFDocument, StandardFonts, rgb } = PDFLib

    const maxItemsPerPage = 25; // Nombre maximal d'éléments par page
	const fontSize = 20;
	const pageHeight = 842; // Hauteur de la page A4 en points
	const marginTop = 50; // Marge supérieure
	const marginBottom = 50; // Marge inférieure

	async function createPdf() {
		// Create a new PDFDocument
		const pdfDoc = await PDFDocument.create();

		// Embed the Times Roman font
		const HelveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

		// Recuperation de la liste en cours
		const elems = document.getElementsByTagName('span');
		const numElements = elems.length;

		let currentPage = 1;
		let y = pageHeight - marginTop; // Commencez en haut de la page
		let page = pdfDoc.addPage([595, 842]); // Page A4 (portrait)

		// Ajoutez le titre "Liste de courses" sur la première page
		if (currentPage === 1) {
			const titleText = "Liste de courses";
			const titleWidth = HelveticaFont.widthOfTextAtSize(titleText, fontSize + 5);

			page.drawText(titleText, {
			x: (page.getWidth() - titleWidth) / 2, // Centrez le titre horizontalement
			y: pageHeight - marginTop,
			size: fontSize + 5, // Taille de police plus grande pour le titre
			color: rgb(0, 0, 0),
			font: HelveticaFont,
			});
			y -= fontSize + 10; // Descendez après le titre
		}

		for (let i = 0; i < numElements; i++) {
		const tasks = elems[i].innerText;
		const elem = i + 1;

		// Vérifiez si nous avons suffisamment d'espace pour un nouvel élément
		if (y - (fontSize + 10) < marginBottom) {
			// Créez une nouvelle page
			currentPage++;
			y = pageHeight - marginTop; // Réinitialisez la position Y
			page = pdfDoc.addPage([595, 842]);
			page.drawText(`Page ${currentPage}`, {
			x: 50,
			y: pageHeight - 20, // Position de l'en-tête en bas de page
			size: fontSize,
			color: rgb(0, 0, 0),
			font: HelveticaFont,
			});
		}

		// Dessinez l'élément
		page.drawText(`(${elem}) - ${tasks} --- [   ]`, {
			x: 50,
			y: y - (fontSize + 10), // Descendez pour chaque élément
			size: fontSize,
			color: rgb(0, 0, 0),
			font: HelveticaFont,
		});

		y -= fontSize + 10; // Mettez à jour la position Y
		}

		// Serialize the PDFDocument to bytes (a Uint8Array)
		const pdfBytes = await pdfDoc.save();

		// Trigger the browser to download the PDF document
		download(pdfBytes, "liste.pdf", "application/pdf");
	}

	// Date et Heure footer
	function refresh() {
		setTimeout('showDate()',1000)
	}
	function showDate() {
		let date = new Date()
		let day = date.getDate();
		let month = date.getMonth();
		let year = date.getFullYear();
		let h = date.getHours();
		let m = date.getMinutes();
		let s = date.getSeconds();
		if( h < 10 ){ h = '0' + h; }
		if( m < 10 ){ m = '0' + m; }
		if( s < 10 ){ s = '0' + s; }
		//let time = 'day + '/' + mon + '-' + h + ':' + m + ':' + s'
		let time = `${day}/${month+1}/${year} - ${h}:${m}:${s}`
		document.getElementById('horloge').innerHTML = time;
		refresh();
	}

	document.addEventListener('DOMContentLoaded', function() {
	showDate();
	});
</script>
</body>
</html>
