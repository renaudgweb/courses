<?php
  $dbhost = 'localhost';
  $dbname = 'bd_xxx';   // à..
  $dbuser = 'xxx';   // ..modifier..
  $dbpass = 'xxx';   // ..selon
  $port   = '3306';

  $bdd = new mysqli($dbhost, $dbuser, $dbpass, $dbname, $port);
  if ($bdd->connect_error) die("Erreur fatale 1");

  function createTable($name, $query)
  { // Créer une table
    // Imposer utf8
    queryMysql("CREATE TABLE IF NOT EXISTS $name($query) CHARSET utf8");
    echo "Table '$name' créée ou existe déjà.<br />";
  }

  function queryMysql($query)
  { // Lance une requete SQL
    global $bdd;
    $result = $bdd->query($query);
    if (!$result) die("Erreur fatale 2");
    return $result;
  }
?>
