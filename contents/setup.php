<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Préparation des tables de la base de données</title>
  </head>
  <body>

    <h3>Préparation des tables de la base de données en cours...</h3>

<?php
  require_once 'functions.php';
  // Tables des messages
  CreateTable('tasks',
              'id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
              task VARCHAR(255)');
?>

  <br />...Terminée.
  </body>
</html>
